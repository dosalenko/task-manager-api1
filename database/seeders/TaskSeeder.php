<?php

namespace Database\Seeders;

use App\Models\Task;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker = Faker::create();

        $userIds = User::pluck('id')->toArray();
        foreach (range(1, 10) as $index) {
            Task::create([
                'user_id' => $faker->randomElement($userIds),
                'title' => $faker->sentence,
                'description' => $faker->paragraph,
                'priority' => $faker->numberBetween(1, 5),
                'status' => $faker->numberBetween(0, 1),
                'completed_at' => $faker->dateTimeThisMonth(),
            ]);
        }
    }
}
