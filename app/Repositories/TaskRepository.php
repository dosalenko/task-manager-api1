<?php

namespace App\Repositories;

use App\Models\Task;

class TaskRepository
{
    public function getAll()
    {
        return Task::all();
    }

    public function findById($id)
    {
        return Task::findOrFail($id);
    }

    public function getTasks($status, $priority, $searchTerm, $sortBy)
    {
        $query = Task::query();

        if ($status) {
            $query->where('status', $status);
        }

        if ($priority) {
            $query->where('priority', $priority);
        }

        if ($searchTerm) {
            $query->where(function ($q) use ($searchTerm) {
                $q->where('title', 'like', '%' . $searchTerm . '%')
                    ->orWhere('description', 'like', '%' . $searchTerm . '%');
            });
        }

        if ($sortBy) {
            $sortParams = explode(',', $sortBy);
            foreach ($sortParams as $param) {
                $param = explode(' ', $param);
                $query->orderBy($param[0], $param[1]);
            }
        }

        return $query->get()->toArray();
    }
}
