<?php

namespace App\Services;

use App\DTO\TaskDTO;
use App\Models\Task;

class TaskService
{
    public function createTask(TaskDTO $taskDTO): Task
    {
        $data = [
            'title' => $taskDTO->title,
            'description' => $taskDTO->description,
            'priority' => $taskDTO->priority,
            'status' => $taskDTO->status,
            'completed_at' => $taskDTO->completed_at,
        ];

        return Task::create($data);
    }

    public function updateTask(Task $task, TaskDTO $taskDTO): Task
    {
        $data = [
            'title' => $taskDTO->title,
            'description' => $taskDTO->description,
            'priority' => $taskDTO->priority,
            'status' => $taskDTO->status,
            'completed_at' => $taskDTO->completed_at,
        ];

        $task->update($data);

        return $task;
    }
}
