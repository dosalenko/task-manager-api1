<?php

namespace App\DTO;

class TaskDTO
{
    public $title;
    public $description;
    public $priority;
    public $status;
    public $completed_at;

    public function __construct(string $title, ?string $description, int $priority, int $status, ?string $completed_at)
    {
        $this->title = $title;
        $this->description = $description;
        $this->priority = $priority;
        $this->status = $status;
        $this->completed_at = $completed_at;
    }
}
