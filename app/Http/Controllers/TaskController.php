<?php

namespace App\Http\Controllers;

use App\DTO\TaskDTO;
use App\Enums\TaskStatus;
use Illuminate\Http\Request;
use App\Models\Task;
use App\Services\TaskService;
use App\Repositories\TaskRepository;
use Illuminate\Http\JsonResponse;

/**
 * @OA\Info(
 *     title="Task Manager API",
 *     description="API documentation for managing tasks",
 *     version="1.0.0",
 * )
 */
class TaskController extends Controller
{
    protected $taskService;
    protected $taskRepository;

    public function __construct(TaskService $taskService, TaskRepository $taskRepository)
    {
        $this->taskService = $taskService;
        $this->taskRepository = $taskRepository;
    }

    /**
     * @OA\Get(
     *     path="/api/tasks",
     *     summary="Retrieve list of tasks",
     *     description="Returns a list of tasks based on provided filters and sorting parameters.",
     *     @OA\Response(response="200", description="List of tasks"),
     * )
     */
    public function index(Request $request): JsonResponse
    {
        $status = $request->input('status');
        $priority = $request->input('priority');
        $searchTerm = $request->input('search');
        $sortBy = $request->input('sort_by');

        $tasks = $this->taskRepository->getTasks($status, $priority, $searchTerm, $sortBy);

        $tasksWithChildren = $this->buildTree($tasks);

        return response()->json($tasksWithChildren);
    }


    /**
     * Recursively build tree structure for tasks.
     *
     * @param \Illuminate\Database\Eloquent\Collection $tasks
     * @return array
     */
    private function buildTree($tasks, $parentId = null): array
    {
        $result = [];

        foreach ($tasks as $task) {
            if ($task['parent_id'] === $parentId) {
                $children = $this->buildTree($tasks, $task['id']);
                if ($children) {
                    $task['children'] = $children;
                }
                $result[] = $task;
            }
        }

        return $result;
    }

    /**
     * @OA\Post(
     *     path="/api/tasks",
     *     summary="Create a new task",
     *     description="Creates a new task with the provided data.",
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             @OA\Property(property="title", type="string", example="Task Title"),
     *             @OA\Property(property="description", type="string", example="Task Description"),
     *             @OA\Property(property="priority", type="integer", example="1"),
     *             @OA\Property(property="status", type="integer", example="0"),
     *             @OA\Property(property="completed_at", type="string", format="date-time", example="2022-05-25 10:00:00"),
     *         )
     *     ),
     *     @OA\Response(response="201", description="Task created successfully"),
     * )
     */
    public function store(Request $request): JsonResponse
    {
        try {
            $data = $request->validate([
                'title' => 'required|string|max:255',
                'description' => 'nullable|string',
                'priority' => 'required|integer',
                'status' => 'required|enum:' . TaskStatus::class,
                'completed_at' => 'nullable|date',
            ]);

            $dto = new TaskDTO(
                $data['title'],
                $data['description'],
                $data['priority'],
                $data['status'],
                $data['completed_at']
            );

            $task = $this->taskService->createTask($dto);

            return response()->json($task, 201);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * @OA\Put(
     *     path="/api/tasks/{id}",
     *     summary="Update an existing task",
     *     description="Updates an existing task with the provided data.",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of the task to update",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             @OA\Property(property="title", type="string", example="Updated Task Title"),
     *             @OA\Property(property="description", type="string", example="Updated Task Description"),
     *             @OA\Property(property="priority", type="integer", example="2"),
     *             @OA\Property(property="status", type="integer", example="1"),
     *             @OA\Property(property="completed_at", type="string", format="date-time", example="2022-05-26 12:00:00"),
     *         )
     *     ),
     *     @OA\Response(response="200", description="Task updated successfully"),
     * )
     */
    public function update(Request $request, int $id): JsonResponse
    {
        try {
            $task = $this->taskRepository->findById($id);

            $data = $request->validate([
                'title' => 'required|string|max:255',
                'description' => 'nullable|string',
                'priority' => 'required|integer',
                'status' => 'required|enum:' . TaskStatus::class,
                'completed_at' => 'nullable|date',
            ]);

            $dto = new TaskDTO(
                $data['title'],
                $data['description'],
                $data['priority'],
                $data['status'],
                $data['completed_at']
            );

            $task = $this->taskService->updateTask($task, $dto);

            return response()->json($task, 200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * @OA\Delete(
     *     path="/api/tasks/{id}",
     *     summary="Delete an existing task",
     *     description="Deletes an existing task with the provided ID.",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of the task to delete",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(response="204", description="Task deleted successfully"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Cannot delete completed task"),
     * )
     */
    public function destroy($id)
    {
        $task = Task::findOrFail($id);

        if ($task->user_id !== auth()->id()) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        if ($task->completed_at) {
            return response()->json(['error' => 'Cannot delete completed task'], 403);
        }

        $task->delete();

        return response()->json(null, 204);
    }

    /**
     * @OA\Put(
     *     path="/api/tasks/{id}/complete",
     *     summary="Mark a task as completed",
     *     description="Marks an existing task as completed.",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of the task to mark as completed",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(response="200", description="Task marked as completed successfully"),
     *     @OA\Response(response="401", description="Unauthorized"),
     * )
     */
    public function complete($id)
    {
        $task = Task::findOrFail($id);

        if ($task->user_id !== auth()->id()) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $task->completed_at = now();
        $task->save();

        return response()->json($task, 200);
    }

    /**
     * Define the schema for the request body separately.
     *
     * @OA\Schema(
     *     schema="TaskRequestBody",
     *     title="Task Request Body",
     *     required={"title", "priority", "status"},
     *     @OA\Property(property="title", type="string", example="Task Title"),
     *     @OA\Property(property="description", type="string", example="Task Description"),
     *     @OA\Property(property="priority", type="integer", example="1"),
     *     @OA\Property(property="status", type="integer", example="0"),
     *     @OA\Property(property="completed_at", type="string", format="date-time", example="2022-05-25 10:00:00"),
     * )
     */
}

