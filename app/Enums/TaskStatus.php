<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

class TaskStatus extends Enum
{
    const Pending = 0;
    const Completed = 1;
}
