# Task Manager API

This Laravel application provides a RESTful API for managing tasks. Users can create, retrieve, update, and delete tasks, as well as mark tasks as completed. Tasks can have unlimited levels of nesting for subtasks.

## Features
- CRUD operations for tasks
- Unlimited levels of nesting for subtasks
- User authentication
- OpenAPI documentation
- Docker Compose setup

## Prerequisites
- PHP (>= 7.4)
- Composer
- Docker
- Docker Compose

## Installation
1. **Navigate to the project directory:**
    ```bash
    cd task-manager-api
    ```

2. **Install PHP dependencies:**
    ```bash
    composer install
    ```

3. **Copy the `.env.example` file and rename it to `.env`:**
    ```bash
    cp .env.example .env
    ```

4. **Generate the application key:**
    ```bash
    php artisan key:generate
    ```

5. **Start the Docker containers:**
    ```bash
    docker-compose up -d
    ```

6. **Run database migrations:**
    ```bash
    docker-compose exec app php artisan migrate
    ```

7. **Access the application at [http://localhost:8000](http://localhost:8000).**
